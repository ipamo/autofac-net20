Autofac for .NET 2.0 and .NET 3.5
=================================

This repository helps to build NuGet package for Autofac 1.3.2, which is the last known version of Autofac compatible with .NET 2.0 and .NET 3.5.

The binaries are available on [nuget.org](https://www.nuget.org/packages/Net20.Autofac).

See also [usage samples](samples/).

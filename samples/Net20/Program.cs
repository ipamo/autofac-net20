﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using Autofac.Builder;
using System.Diagnostics;

namespace Net20
{
    class Program
    {
        static void Main(string[] args)
        {
            var sw = new Stopwatch();
            sw.Start();

            var builder = new ContainerBuilder();
            builder.Register<ISomeServiceA>(new SomeService()); // Singleton
            builder.Register(typeof(SomeService)).As<ISomeServiceB>().FactoryScoped(); // Transient
            builder.Register(typeof(SomeService)).As<ISomeServiceC>().ContainerScoped(); // Scoped
            builder.RegisterGeneric(typeof(PrefixService<>)).SingletonScoped();
            var container = builder.Build();

            var serviceA1 = container.Resolve<ISomeServiceA>();
            Test("Hello, World 1!", serviceA1.Hello);

            var serviceA2 = container.Resolve<ISomeServiceA>();
            Test("Hello, World 1!", serviceA2.Hello);

            var serviceB1 = container.Resolve<ISomeServiceB>();
            Test("Hello, World 2!", serviceB1.Hello);

            var serviceB2 = container.Resolve<ISomeServiceB>();
            Test("Hello, World 3!", serviceB2.Hello);

            var serviceC1 = container.Resolve<ISomeServiceC>();
            Test("Hello, World 4!", serviceC1.Hello);

            var serviceC2 = container.Resolve<ISomeServiceC>();
            Test("Hello, World 4!", serviceC2.Hello);

            using (var subContainer = container.CreateInnerContainer())
            {
                var serviceC3 = subContainer.Resolve<ISomeServiceC>();
                Test("Hello, World 5!", serviceC3.Hello);

                var serviceC4 = subContainer.Resolve<ISomeServiceC>();
                Test("Hello, World 5!", serviceC4.Hello);
            }

            var serviceC5 = container.Resolve<ISomeServiceC>();
            Test("Hello, World 4!", serviceC5.Hello);

            var prefixServiceInt = container.Resolve<PrefixService<int>>();
            Test("33 Hello, World 1!", prefixServiceInt.Prefix(33));

            var prefixServiceString = container.Resolve<PrefixService<string>>();
            Test("Trente-trois Hello, World 1!", prefixServiceString.Prefix("Trente-trois"));

            Console.WriteLine(sw.ElapsedMilliseconds);
            Console.ReadKey();
        }

        private static void Test(string expected, string actual)
        {
            if (expected == actual)
                Console.WriteLine($"[OK] {actual}");
            else
                Console.WriteLine($"[ERROR] {actual} (expected: {expected})");
        }

        private interface ISomeServiceA
        {
            string Hello { get; }
        }

        private interface ISomeServiceB
        {
            string Hello { get; }
        }

        private interface ISomeServiceC
        {
            string Hello { get; }
        }

        private class SomeService : ISomeServiceA, ISomeServiceB, ISomeServiceC
        {
            private static int _globalNum = 1;
            private int _num = 1;

            public SomeService()
            {
                _num = _globalNum++;
            }

            public string Hello => $"Hello, World {_num}!";
        }

        private class PrefixService<T>
        {
            private ISomeServiceA _politeService;

            public PrefixService(ISomeServiceA politeService)
            {
                _politeService = politeService;
            }

            public string Prefix(T prefix)
            {
                return $"{prefix} {_politeService.Hello}";
            }
        }
    }
}
